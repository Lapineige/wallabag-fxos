/* utils.js
* Utilities for UI things
*/

(function(w) {
  "use strict";
  /// UI
  function spinner(spin) {
    var i = document.getElementById("indicateRefresh");
    if (spin) {
      i.classList.add("fa-spin");
    } else {
      i.classList.remove("fa-spin");
    }
  }

  // UTILS:
  //XXX use addURLPromises as FIFO!!

  w.utils = {
    spinner: spinner
  };
})(window);
